<?php
/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $options['type'] will either be ul or ol.
 * @ingroup views_templates
 */

if (!empty($title)) {
    print $wrapper_prefix;
            print '<h2>' .  $title . '</h2>';
            if (!empty($rows)) :
                ?>
                <?php print $list_type_prefix; ?>
                <?php foreach ($rows as $id => $row): ?>
                <li class="<?php print $classes_array[$id]; ?>"><?php print $row; ?></li>
            <?php endforeach; ?>
            <?php print $list_type_suffix; ?>
        <?php endif; ?>

    <?php
    print $wrapper_suffix;
} else {
    print $wrapper_prefix;
    if (!empty($rows)) :
        ?>
        <?php print $list_type_prefix; ?>
        <?php foreach ($rows as $id => $row): ?>
            <li class="<?php print $classes_array[$id]; ?>"><?php print $row; ?></li>
        <?php endforeach; ?>
        <?php
        print $list_type_suffix;
    endif;
    print $wrapper_suffix;
}

