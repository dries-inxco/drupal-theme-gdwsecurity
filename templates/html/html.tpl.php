<?php
//if($logged_in == 1):
?>


<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie6 ie" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>"> <![endif]-->
<!--[if IE 7]>    <html class="ie7 ie" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>"> <![endif]-->
<!--[if IE 8]>    <html class="ie8 ie" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>"> <![endif]-->
<!--[if gt IE 8]> <!--> <html class="" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>"> <!--<![endif]-->
    <head>
        <?php $themejspath = drupal_get_path('theme', 'gdwsecurity') . '/js'; ?>
        <?php $locale = $language->language; ?>
        <?php print $head; ?>
        <!-- Set the viewport width to device width for mobile -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title><?php print $head_title; ?></title>
        <?php
        global $language;
        print $styles;


            // string needle NOT found in haystack

            $scripts = drupal_add_js();

            $jquerypath = $themejspath . '/module/jquery/jquery-1.10.2.min.js';
            $migratepath = $themejspath . '/module/jquery/jquery-migrate-1.2.1.min.js';
            $basepath = $themejspath . '/site/base.js';

            $scripts[$migratepath] = $scripts['misc/jquery.js'];
            $scripts[$basepath] = $scripts['misc/jquery.js'];

            $scripts['misc/jquery.js']['data'] = $jquerypath;
            $scripts['misc/jquery.js']['version'] = 1.10;
            $scripts['misc/jquery.js']['weight'] -= 3;

            if (isset($scripts['misc/jquery.form.js'])) {
                $scripts['misc/jquery.form.js']['data'] = $themejspath . '/module/jquery/jquery.form.js';
                $scripts['misc/jquery.form.js']['version'] = 3.51;
            }

            $scripts[$migratepath]['data'] = $migratepath;
            $scripts[$migratepath]['version'] = 1.2;
            $scripts[$migratepath]['weight'] -= 2;

            $scripts[$basepath]['data'] = $basepath;
            $scripts[$basepath]['version'] = 1.0;
            $scripts[$basepath]['weight'] += 100;

            $scripts = drupal_get_js('header', $scripts);

        print $scripts;

        ?>

        <!-- IE Fix for HTML5 Tags -->
        <!--[if lt IE 9]>
          <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    </head>

    <body class="<?php print $classes; ?>" <?php print $attributes; ?> itemscope="" itemtype="http://schema.org/WebPage">
        <?php print $page_top; ?>
        <?php print $page; ?>
        <?php print $page_bottom; ?>
        
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 943945894;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="remarketing" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/943945894/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<script src="//assets.pcrl.co/js/jstracker.min.js"></script>
    </body>

</html>

<?php //endif; ?>