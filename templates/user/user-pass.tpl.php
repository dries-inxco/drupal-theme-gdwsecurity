<?php
if ( $user->uid ) {
  print '<h1>Nieuw wachtwoord instellen</h1>';
  print '<p>' . render($intro_text) . '</p>';
  print drupal_render_children($form);
}
else {
  print '<h1>Wachtwoord vergeten?</h1>';
  print '<p>' . render($intro_text) . '</p>';
  print drupal_render_children($form);

}
?>
