<?php 
global $base_path;
global $language;
?>

<section id="top">
	<div class="container">
		<div class="col s12">
			<h2 class="hidden">Top region</h2>
			<p class="slogan"><?php print $site_slogan; ?></p>
			<?php if ( $page['top'] ) : ?>
				<?php print render($page['top']) ?>
			<?php endif; ?>
		</div>
	</div>
</section>

<header role="banner">
	<div class="container">
		<div class="col s12">
			<h2><a href="<?php print $base_path; ?>" title="<?php print $site_name; ?>" rel="home"><img class="logo" src="<?php print $logo; ?>" alt="<?php print $site_name; ?>" title="<?php print $site_name; ?>"></a></h2>
		</div>
	</div>
	<div class="nav">
		<div class="container">
			<div class="col s12">
				<?php if ( $page['header'] ) : ?>
					<?php print render($page['header']) ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</header>

<main role="main">
	<section id="content">
		<div class="container">
			<div class="col s12">	
				<h2 class="hidden">Main content</h2>
					<?php if ( $page['content'] ) { 
						if (!empty($tabs['#primary'])): ?><div class="tabs-wrapper"><?php print render($tabs); ?></div><?php endif;
						print render($page['content']); 
					} ?>
					<?php if ( $page['content_2'] ) { 
						print render($page['content_2']); 
					} ?>
			</div>
		</div>
	</section>

	<?php if ( $page['promoted'] ) : ?>
	<section id="promoted">
		<div class="container">
			<div class="col s12">	
				<?php print render($page['promoted']) ?>
			</div>
		</div>
	</section>
	<?php endif; ?>
	
	<?php if ( $page['promoted_2'] ) : ?>
	<section id="promoted_2">
		<div class="container">
			<div class="col s12">	
				<?php print render($page['promoted_2']) ?>
			</div>
		</div>
	</section>
	<?php endif; ?>

	<?php if ( $page['promoted_3'] ) : ?>
	<section id="promoted_3">
		<div class="container">
			<div class="col s8">	
				<?php print render(array_values($page['promoted_3'])[0]); ?>

			</div>
			<div class="col s4">	
				<?php 
				if (array_values($page['promoted_3'])[1] != 1){
					print render(array_values($page['promoted_3'])[1]);
				}
				?>
			</div>
		</div>
	</section>
	<?php endif; ?>
	


</main>

<?php if ( $page['footer'] || $page['footer_2'] || $page['footer_3'] || $page['bottom'] ) : ?>
	<footer role="contentinfo">
		<section id="footer">
			<h2 class="hidden">Footer</h2>
			<div class="container">
				<section class="col s4">
					<?php print render($page['footer']) ?>
				</section>
				<section class="col s4">
					<?php print render($page['footer_2']) ?>
				</section>
				<section class="col s4">
					<?php print render($page['footer_3']) ?>
				</section>
			</div>
		</section>
		<section id="bottom">
			<h2 class="hidden">Bottom region</h2>
			<div class="container">
				<div class="col s12">
					<?php print render($page['bottom']) ?>
				</div>
			</div>
		</section>
	</footer>
<?php endif; ?>
