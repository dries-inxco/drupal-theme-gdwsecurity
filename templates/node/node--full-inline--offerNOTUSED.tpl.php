<?php
if (!empty($tabs['#primary'])) : ?>
<div class="tabs-wrapper"><?php print render($tabs); ?></div>
<?php endif; ?>

<article id="<?php print (isset($node->field_uri['und'][0]['value'])?$node->field_uri['und'][0]['value']:'node-' . $node->nid); ?>" class="<?php print $classes; ?>"<?php print $attributes; ?>>

        <header>
            <h2 itemprop="name"><?php print $title; ?></h2>
        </header>

        <?php
        hide($content['comments']);
        hide($content['links']);
        hide($content['field_tags']);     
        print render($content);
        ?>

            <?php 

                // $view = views_get_view('Core');
                // // set active display on the view
                // $view->set_display('text_conv');
                // // set any needed arguments
                // $view->set_arguments(array($node->nid));
                // // execute the view
                // $view->execute();
                // // display the results
                // print $view->preview();
            ?>

</article>
