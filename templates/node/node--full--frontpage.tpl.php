<section>
        <?php
        if (!empty($tabs['#primary'])):
            ?><div class="tabs-wrapper"><?php print render($tabs); ?></div><?php endif; ?>
                <article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>"<?php print $attributes; ?>>

                    <h1 itemprop="name"><?php print $title; ?></h1>
                    <?php
                    // Hide comments, tags, and links now so that we can render them later.
                    hide($content['field_tags']);
                    hide($content['comments']);
                    hide($content['links']);

                    hide($content['field_int_link_label_conv']);
                    hide($content['field_int_link_reference_conv']);       
                    print render($content);

                    //internal link conversion
                    if (isset($node->field_int_link_label_conv['und'])){
                       if ($node->field_int_link_label_conv['und'][0]['value']) {
                            print l($node->field_int_link_label_conv['und'][0]['value'], 'node/' . $node->field_int_link_reference_conv['und'][0]["target_id"], array('attributes' => array('class' => array('button', 'button-special'))));
                        }     
                    }

                    ?>
                </article>
</section>


