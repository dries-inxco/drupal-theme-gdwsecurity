<?php
if (!empty($tabs['#primary'])) {
    ?><div class="tabs-wrapper"><?php print render($tabs); ?></div><?php
}
?>
<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>"<?php print $attributes; ?>>

    <header>
        <h1 itemprop="name"><?php print $title; ?></h1>
    </header>

    <div class="content"<?php print $content_attributes; ?>>
        <?php
        // Hide comments, tags, and links now so that we can render them later.
        hide($content['comments']);
        hide($content['links']);
        hide($content['field_tags']);
        print render($content);
        ?>
    </div>

</article>