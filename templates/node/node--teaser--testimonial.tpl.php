<?php
$name = $node->field_testimonial_name['und'][0]['value'];
$jobtitle = $node->field_testimonial_jobtitle['und'][0]['value'];
$quote = $node->field_testimonial_quote['und'][0]['value'];

?>

<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>"<?php print $attributes; ?>>


        <h2><?php print t('Reference'); ?></h2>

        <?php if ($user_picture || $display_submitted): ?>

                <header>
                <?php print $user_picture; ?>
                <?php if ($display_submitted): ?>
                    <span class="submitted"><?php print $submitted; ?></span>
                <?php endif; ?>
                </header>

        <?php endif; ?>

        <?php hide($content['comments']); ?>            
        <?php hide($content['links']); ?>

        <?php print render($content['field_picture_small']); ?>          
        <?php print render($content['field_quote']); ?>        
        <?php print render($content['field_name']); ?>
        <?php print render($content['field_jobtitle']); ?>

        <p class="center-text"><a href="<?php print $node_url ; ?>" class="button"><?php print t('Continue'); ?></a></p>

</div>
