<?php
if (!empty($tabs['#primary'])) {
    ?><div class="tabs-wrapper"><?php print render($tabs); ?></div><?php
}
?>
<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>"<?php print $attributes; ?>>

    <div class="content"<?php print $content_attributes; ?>>
        <?php
        // Hide comments, tags, and links now so that we can render them later.
        hide($content['comments']);
        hide($content['links']);
        hide($content['field_tags']);
        print render($content);

        ?>
    </div>

</article>
<script>
//  // Close this window after 10 seconds.
//     window.setTimeout(CloseMe, 3000);
//     function CloseMe() {
//         window.parent.location.reload(true);
//         self.close();
//     }
// </script>
