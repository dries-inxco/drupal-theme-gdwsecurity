<?php
$fullStory = $node->field_full_story['und'][0]['value'];
?>


<div id="node-<?php print $node->nid; ?>" class="<?php print ($fullStory == '1'?'full-case':'short-case') . ' ' . $classes; ?>"<?php print $attributes; ?>>
  <?php hide($content['links']); ?>
        <?php print render($content['field_case_image']); ?>

        <div>
            <h2><?php print $title ?></h2>
            <?php print render($content['body']); ?>

            <?php if ($fullStory == '1'){
                print '<p class="center-text"><a href="' . $node_url . '" class="button">' . t('Continue') . '</a></p>';
            }else{
                print render($content['field_related_offer_1']);    
            }
            ?>
           
        </div>

</div>
