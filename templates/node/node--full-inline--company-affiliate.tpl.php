<?php
if (!empty($tabs['#primary'])) : ?>
<div class="tabs-wrapper"><?php print render($tabs); ?></div>
<?php endif; ?>

<article id="<?php print (isset($node->field_uri['und'][0]['value'])?$node->field_uri['und'][0]['value']:'node-' . $node->nid); ?>" class="<?php print $classes; ?>"<?php print $attributes; ?>>

        <header>
            <h2 itemprop="name"><?php print $title; ?></h2>
        </header>

        <?php
        hide($content['comments']);
        hide($content['links']);
        hide($content['field_tags']);     
        //print render($content);

        print '<div>';
        $longitude= $node->field_location['und'][0]['lon'];
        $latitude= $node->field_location['und'][0]['lat'];
        
        // Assume that 0, 0 is invalid.
        if ($latitude != 0 || $longitude != 0) {
        	print '<p>GDW Security nv <span style="color:#ee1122;">/</span> BE 0429.563.114</p>';
            print render($content['field_address']);

            print '<p class="phone">T: ' . $node->field_phone['und'][0]['value'] . ' <span style="color:#ee1122;">/</span> F: ' . $node->field_fax['und'][0]['value'] . '</p>';
            print render($content['field_email']);  

            //print '<p>' . $node->field_street_name['und'][0]['value'] . ' ' . $node->field_street_number['und'][0]['value'] . (isset($node->field_premise['und'][0]['value'])?' (' . $node->field_premise['und'][0]['value'] . ')':'') . '<br>';
            //print $node->field_postal_code['und'][0]['value'] . ' ' . $node->field_locality['und'][0]['value'] . '</p>';
            print gmap_simple_map($latitude, $longitude, $markername = 'blue', $info = '', $zoom = '13', $width = '100%', $height = '260px', $autoshow = FALSE, $map = array('maptype' => "Map", 'nomousezoom' => 'FALSE', 'control'=>'None' ));

        }

        print '</div>';



        ?>

</article>
