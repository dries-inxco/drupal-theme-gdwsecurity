<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>"<?php print $attributes; ?>>

        <h2><?php print $title ?></h2>

        <?php if ($display_submitted): ?>
                <header>
                    <span class="submitted"><?php print strip_tags($submitted); ?></span>
                </header>
        <?php endif; ?>

        <?php hide($content['links']); ?>
        <?php print render($content); ?>

</div>

