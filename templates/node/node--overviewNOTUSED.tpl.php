<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> node-overview"<?php print $attributes; ?>>

        <h2><?php print $title ?></h2>        

        <?php if ($display_submitted): ?>
                <header>
                    <span class="submitted"><?php print $submitted; ?></span>
                </header>
        <?php endif; ?>

        <?php hide($content['links']); ?>
        <?php print render($content); ?>
        <p class="center-text"><a href="<?php print $node_url ; ?>" class="button"><?php print t('Continue'); ?></a></p>
        
</div>