<?php

if (!empty($tabs['#primary'])) {
    ?><div class="tabs-wrapper"><?php print render($tabs); ?></div><?php
}
?>
            <header>
                <h1 itemprop="name"><?php print $title; ?></h1>
            </header>

            <?php
            // Hide comments, tags, and links now so that we can render them later.
            hide($content['comments']);
            hide($content['links']);
            hide($content['field_tags']);
            //print render($content);

            //count number of jobs
            $jobs_count = db_select('node')
              ->condition('type', 'job')
              ->condition('status', '1')
              ->countQuery()->execute()->fetchField();

            if ($jobs_count == 0){
              print render($content['field_no_results_text']);     
            }
            else{
              print render($content['body']);
            }
            
            ?>

