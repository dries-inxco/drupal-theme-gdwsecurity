<?php

//print theme('breadcrumb', array('breadcrumb' => drupal_get_breadcrumb()));

if (!empty($tabs['#primary'])) {
    ?><div class="tabs-wrapper"><?php print render($tabs); ?></div><?php
}
?>


        <header>
            <h1 itemprop="name"><?php print $title; ?></h1>

            <?php if ($display_submitted): ?>
                <p class="submitted"><?php print $submitted; ?></p>
            <?php endif; ?>
            
        </header>

        <?php
        hide($content['comments']);
        hide($content['links']);
        hide($content['field_tags']);     
        print render($content);

        ?>

        <?php if ( $region = render($conversion_region) ) : ?>
            <aside id="conversion">
                <?php print $region; ?>
            </aside>
        <?php  endif; ?>

