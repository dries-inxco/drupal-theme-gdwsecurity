<?php
if (!empty($tabs['#primary'])) : ?>
<div class="tabs-wrapper"><?php print render($tabs); ?></div>
<?php endif; ?>

<article id="<?php print (isset($node->field_uri['und'][0]['value'])?$node->field_uri['und'][0]['value']:'node-' . $node->nid); ?>" class="<?php print $classes; ?>"<?php print $attributes; ?>>

        <header>
            <h3 itemprop="name"><?php print $title; ?></h3>
        </header>

        <?php
        hide($content['comments']);
        hide($content['links']);
        hide($content['field_tags']);     
        print render($content);
        ?>

</article>
