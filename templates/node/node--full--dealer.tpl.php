<?php

if (!empty($tabs['#primary'])) {
    ?><div class="tabs-wrapper"><?php print render($tabs); ?></div><?php
}
?>
<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>"<?php print $attributes; ?>>

        <header>
            <h1 itemprop="name"><?php print $title; ?></h1>
        </header>

        <?php
        hide($content['comments']);
        hide($content['links']);
        hide($content['field_tags']);  

        print render($content['field_dealer_logo']);
        print render($content['body']);


            print '<div>';
            $longitude= $node->field_location['und'][0]['lon'];
            $latitude= $node->field_location['und'][0]['lat'];
            
            // Assume that 0, 0 is invalid.
            if ($latitude != 0 || $longitude != 0) {
                print '<h2>Contactgegevens</h2>';
                print $node->title;
                print render($content['field_address']);
                print render($content['field_dealer_phone']);
                print render($content['field_dealer_email']); 
                print render($content['field_url']);  

                //print '<p>' . $node->field_street_name['und'][0]['value'] . ' ' . $node->field_street_number['und'][0]['value'] . (isset($node->field_premise['und'][0]['value'])?' (' . $node->field_premise['und'][0]['value'] . ')':'') . '<br>';
                //print $node->field_postal_code['und'][0]['value'] . ' ' . $node->field_locality['und'][0]['value'] . '</p>';
                print gmap_simple_map($latitude, $longitude, $markername = 'blue', $info = '', $zoom = '13', $width = '100%', $height = '200px', $autoshow = FALSE, $map = array('maptype' => "Terrain", 'nomousezoom' => 'TRUE'));

            }

            print '</div>';

    //related case
    // if (isset($node->field_case['und'])){
    //     print '<div class="dealer container">';
    //       print '<h2>Gerelateerde realisaties</h2>';
    //       print '<div class="item-list"><ul>';
    //        foreach ($node->field_case['und'] as $key => $value) {
    //           //var_dump($value['entity']->title);
    //           print '<li>' . l($value['entity']->title, 'referenties', array('fragment' => 'node-' . $value['target_id'])) . '</li>';
    //       }
    //       print '</ul></div>';
    //     print '</div>';
    // }




        ?>



</article>


