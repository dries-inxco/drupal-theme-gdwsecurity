<?php

function gdwsecurity_css_alter(&$css) {

    //make a list of module css to remove
    $css_to_remove = array(
        'misc/ui/jquery.ui.accordion.css',
        'misc/ui/jquery.ui.core.css',
        'misc/ui/jquery.ui.theme.css',
        'modules/system/system.menus.css',
        'modules/system/system.theme.css',
        'modules/system/system.messages.css',
        'modules/system/system.base.css',
        'modules/field/theme/field.css',
        'modules/node/node.css',
        'modules/search/search.css',
        'modules/shortcut/shortcut.css',   
        'modules/user/user.css',     
        'sites/all/modules/date/date_popup/themes/datepicker.1.7.css',
        'sites/all/modules/access_unpublished/css/access_unpublished.css',
        'sites/all/modules/date/date_api/date.css',
        'sites/all/modules/views/css/views.css',
        'sites/all/modules/ctools/css/ctools.css',               
    );
    // now we can remove the contribs from the array
    foreach ($css_to_remove as $index => $css_file) {
        unset($css[$css_file]);
    }

}

function gdwsecurity_preprocess_html(&$vars) {


    if ($node = menu_get_object()) {
        if (isset($node->field_delta['und'][0]['value'])) {
            $vars['classes_array'][] = drupal_html_class('node-type-' . $node->type . '-' . $node->field_delta['und'][0]['value']);
        }
    }

}

function gdwsecurity_preprocess_node(&$vars) {

    $length = count($vars['theme_hook_suggestions']);

    //viewmode template
    $vars['theme_hook_suggestions'][] = 'node__' . $vars['view_mode'];

    //general section template
    if ((strpos($vars['node']->type,'section') !== false) && ($vars['view_mode'] == 'full')) {
        $vars['theme_hook_suggestions'][] = 'node__' . $vars['view_mode'] . '__section';
    }

    //contenttype template
    $vars['theme_hook_suggestions'][] = 'node__' . $vars['view_mode'] . '__' . $vars['type'];

    //contenttype + delta template
    if (isset($vars['node']->field_delta['und'][0]['value'])) {
        $vars['theme_hook_suggestions'][] = 'node__' . $vars['view_mode'] . '__' . $vars['type'] . '__' . $vars['node']->field_delta['und'][0]['value'];
    }

    //override submitted variable
    if (variable_get('node_submitted_' . $vars['node']->type, TRUE)) {
        $date = format_date($vars['node']->created, 'short');
        $vars['submitted'] = t('!datetime', array('!datetime' => $date));
    }

    //make regions available in node tpl
    if ($vars['view_mode'] == 'full')  {
        // if ($blocks = block_get_blocks_by_region('related')) {
        //     $vars['related_region'] = $blocks;
        // }
        if ($blocks = block_get_blocks_by_region('conversion')) {
            $vars['conversion_region'] = $blocks;
        }
    }
}

//function gdwsecurity_preprocess_region(&$vars) {
    // if ($blocks = block_get_blocks_by_region('side')) {
    //     $vars['side_region'] = $blocks;
    // }
    // if ($blocks = block_get_blocks_by_region('subnavigation')) {
    //     $vars['subnavigation_region'] = $blocks;
    // }
//    dpm($vars['theme_hook_suggestions']);
//}

function gdwsecurity_preprocess_block(&$vars) {
    $block = $vars['block'];
    $length = count($vars['theme_hook_suggestions']);
    $length2 = count($vars['classes_array']);
    $block_namesegments = explode('-', $block->delta);
    $block_name = array_shift($block_namesegments);
    if (count($block_namesegments) < 1) {
        array_splice($vars['theme_hook_suggestions'], $length - 1, 0, array(
            'block__' . $block->region . '__' . $block->module
        ));
        array_splice($vars['classes_array'], 1, 0, array(
            'block-' . $block->region
        ));
        array_splice($vars['classes_array'], $length2, 0, array(
            'block-' . $block->region . '-' . $block->module
        ));
    } else {
        $block_namesegments = implode('-', $block_namesegments);
        array_splice($vars['theme_hook_suggestions'], $length - 1, 0, array(
            'block__' . $block->region . '__' . $block->module,
            'block__' . $block->region . '__' . $block_namesegments,
            'block__' . $block->module . '__' . $block_namesegments,
            'block__' . $block->region . '__' . $block->module . '__' . $block_namesegments
        ));
        array_splice($vars['classes_array'], 1, 0, array(
            'block-' . $block->region
        ));
        array_splice($vars['classes_array'], $length2, 0, array(
            'block-' . $block->region . '-' . $block->module,
            'block-' . $block->region . '-' . $block_namesegments,
            'block-' . $block->module . '-' . $block_namesegments,
            'block-' . $block->region . '-' . $block->module . '-' . $block_namesegments
        ));
    }
}

/**
 * Changes the search form to use the "search" input element of HTML5.
 */
function gdwsecurity_preprocess_search_block_form(&$vars) {
    $vars['search_form'] = str_replace('type="text"', 'type="search"', $vars['search_form']);
}

function gdwsecurity_menu_tree__menu_block(&$variables) {
    //var_export($variables);
    return '<ul class="nav">' . $variables['tree'] . '</ul>';
}

function gdwsecurity_menu_link(array $variables) {
    $element = $variables['element'];
    //add menu name as a class attribute to the li element
    array_push($element['#attributes']['class'], $element['#original_link']['menu_name']);

    $sub_menu = '';

    if ($element['#below']) {
        // Wrap in dropdown-menu.
        unset($element['#below']['#theme_wrappers']);
        $sub_menu = '<ul>' . drupal_render($element['#below']) . '</ul>';
    }
    $output = l($element['#title'], $element['#href'], $element['#localized_options']);
    return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}

function gdwsecurity_links__system_main_menu($variables) {
    $links = $variables['links'];
    $attributes = $variables['attributes'];
    $heading = $variables['heading'];
    global $language_url;
    $output = '';

    if (count($links) > 0) {
        // Treat the heading first if it is present to prepend it to the
        // list of links.
        if (isset($variables['title_prefix']) && !empty($variables['title_prefix'])) {
            $output .= render($variables['title_suffix']);
        }
        if (!empty($heading)) {
            if (is_string($heading)) {
                // Prepare the array that will be used when the passed heading
                // is a string.
                $heading = array(
                    'text' => $heading,
                    // Set the default level of the heading.
                    'level' => 'h2',
                );
            }
            if ($heading['text'] !== '') {
                $output .= '<' . $heading['level'];
                if (!empty($heading['class'])) {
                    $output .= drupal_attributes(array('class' => $heading['class']));
                }
                $output .= '>' . check_plain($heading['text']) . '</' . $heading['level'] . '>';
            }
        }
        if (isset($variables['title_suffix']) && !empty($variables['title_suffix'])) {
            $output .= render($variables['title_suffix']);
        }

        if (!isset($attributes['class'])) {
            $attributes['class'] = array();
        }

        array_push($attributes['class'], 'nav');

        $output .= '<ul' . drupal_attributes($attributes) . '>';

        $num_links = count($links);
        $i = 1;

        foreach ($links as $key => $link) {
            $class = array($key);

            // Add first, last and active classes to the list of links to help out themers.
            if ($i == 1) {
                $class[] = 'first';
            }
            if ($i == $num_links) {
                $class[] = 'last';
            }
            if (isset($link['href']) && ($link['href'] == $_GET['q'] || ($link['href'] == '<front>' && drupal_is_front_page())) && (empty($link['language']) || $link['language']->language == $language_url->language)) {
                $class[] = 'active';
            }
            $output .= '<li' . drupal_attributes(array('class' => $class)) . '>';

            if (isset($link['href'])) {
                // Pass in $link as $options, they share the same keys.
                $output .= l($link['title'], $link['href'], $link);
            } elseif (!empty($link['title'])) {
                // Some links are actually not links, but we wrap these in <span> for adding title and class attributes.
                if (empty($link['html'])) {
                    $link['title'] = check_plain($link['title']);
                }
                $span_attributes = '';
                if (isset($link['attributes'])) {
                    $span_attributes = drupal_attributes($link['attributes']);
                }
                $output .= '<span' . $span_attributes . '>' . $link['title'] . '</span>';
            }

            $i++;
            $output .= "</li>\n";
        }

        $output .= '</ul>';
    }
    return $output;
}

/*
  function gdwsecurity_preprocess_textfield(&$variables) {
  if ($variables['element']['#required']) {
  $variables['element']['#attributes']['data-required'] = "true";
  }
  $variables['element']['#ajax_processed'] = true;
  dpm($variables['element']);
  }

 */

function gdwsecurity_form_alter(&$form, &$form_state, $form_id) {
    if (!empty($form['actions']) && $form['actions']['submit']) {
        if (isset($form['actions']['submit']['#attributes']['class'])) {
            $form['actions']['submit']['#attributes']['class'] = array_merge($form['actions']['submit']['#attributes']['class'], array('button', 'button-special'));
        } else {
            $form['actions']['submit']['#attributes']['class'] = array('button', 'button-special');
        }
    }

    if ($form_id == 'user_login') {

        $form['links']['#markup'] = l('Wachtwoord vergeten','user/password'); // Remove Request New Password from Block form

    }

}

function gdwsecurity_html_head_alter(&$head_elements) {

    //binding the og:description to the meta description
    if (!isset($head_elements['metatag_og:description_0'])) {

        if (isset($head_elements['metatag_description_0']['#value'])) {

            $meta_description = $head_elements['metatag_description_0']['#value'];

            $head_elements['metatag_og:description_0'] = array(
                '#theme' => 'metatag_property',
                '#tag' => 'meta',
                '#id' => 'metatag_og:description_0',
                '#name' => 'og:description',
                '#value' => $meta_description,
                '#type' => 'html_tag',
            );
        }
    }

    //no-index robots tag on paginated urls or staging version
    if (!isset($head_elements['metatag_robots'])) {

        if (isset($_GET['page']) || isset($_GET['tag']) || isset($_GET['year']) || variable_get('dev', NULL) == 1) {

            $head_elements['metatag_robots'] = array(
                '#theme' => 'metatag',
                '#tag' => 'meta',
                '#id' => 'metatag_robots',
                '#name' => 'robots',
                '#value' => 'noindex',
                '#type' => 'html_tag',
            );
        }
    }
}

function gdwsecurity_preprocess_page(&$vars) {

    //add page template suggestions
    if (isset($vars['node']->type)) {

        $page_template = variable_get('webdna_page_template_' . $vars['node']->type, 'default');

        if($page_template != 'default'){
            $vars['theme_hook_suggestions'][] = 'page__' . $page_template;
        }

        //contenttype + delta template
        if (isset($vars['node']->field_delta['und'][0]['value'])) {
            $vars['theme_hook_suggestions'][] = 'page__' . $page_template . '__' . $vars['node']->type . '__' . $vars['node']->field_delta['und'][0]['value'];
        }
    }



    /* plugins inladen */
    drupal_add_library('system', 'jquery.cookie');

    global $user;  
 
    if ($user->uid !== 0){
        // Add the new css.
        drupal_add_css (path_to_theme() . '/css/admin.css', array('type' => 'file'));
    }




}

function gdwsecurity_breadcrumb($variables) {

    $breadcrumb = $variables['breadcrumb'];

    if (!empty($breadcrumb) && count($breadcrumb) > 1) {

        // $breadcrumb_separator = '<span class="breadcrumb-separator">&#xe603;</span>';
        // $output .= '<div class="breadcrumb">' . implode($breadcrumb_separator, $breadcrumb) . '</div>';

        $output = '<ol itemscope itemtype="http://schema.org/BreadcrumbList">';

        foreach ($breadcrumb as $index => $crumb_item) {

            //label
            $label = strip_tags($crumb_item);

            //get link url
            if (strpos($crumb_item,'href') !== false){
                $s = explode('href="',$crumb_item);
                $t = explode('">',$s[1]);    
                $link = '<a title="' . $label . '" href="' . $t[0] . '"><span itemprop="name">' . $label . '</span></a>';
            }else{
                $link = $label;
            }


            // if($t[0] == ''){
                
            // }else{
                

            //get link label
            $link_label = strip_tags($crumb_item);

            $output .= '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">' . $link . '<meta itemprop="position" content="' . ($index + 1) . '" /></li>';
        }

        $output .= '</ol>';

        return $output;
    }
}


function gdwsecurity_file_link($variables) {    
  $file = $variables['file'];
  $icon_directory = $variables['icon_directory'];
 
  $url = file_create_url($file->uri);
  $icon = theme('file_icon', array('file' => $file, 'icon_directory' => $icon_directory, 'alt' => 'PDF icon'));
 
  // Set options as per anchor format described at
  // http://microformats.org/wiki/file-format-examples
  $options = array(
    'attributes' => array(
      'type' => $file->filemime . '; length=' . $file->filesize,
    ),
  );

  // Use the description as the link text if available.
  if (empty($file->description)) { 
    $link_text = $file->filename;
  }
  else {
    $link_text = $file->description;
    //$options['attributes']['title'] = check_plain($file->filename);
  }

  $options['attributes']['title']  = t('External link in new window');
  
  //open files of particular mime types in new window
  $new_window_mimetypes = array('application/pdf','text/plain');
  if (in_array($file->filemime, $new_window_mimetypes)) {
    $options['attributes']['target'] = '_blank';
  }
 
  return $icon . ' ' . l($link_text, $url, $options) . ' (' . format_size($file->filesize,'kB') . ')';
}



function gdwsecurity_theme() {
  $items = array();
    
  $items['user_login'] = array(
    'render element' => 'form',
    'path' => drupal_get_path('theme', 'gdwsecurity') . '/templates/user',
    'template' => 'user-login',

  );

  $items['user_pass'] = array(
    'render element' => 'form',
    'path' => drupal_get_path('theme', 'gdwsecurity') . '/templates/user',
    'template' => 'user-pass',
  );

  $items['user_profile_form'] = array(
    'render element' => 'form',
    'path' => drupal_get_path('theme', 'gdwsecurity') . '/templates/user',
    'template' => 'user-profile-edit',
  );

  return $items;
}


function gdwsecurity_preprocess_user_login(&$variables) {
  $variables['intro_text'] = '';
}

function gdwsecurity_preprocess_user_pass(&$variables) {
  $variables['intro_text'] = 'Vul uw e-mailadres in om zo een nieuw wachtwoord in te stellen.';
}

function gdwsecurity_preprocess_user_profile_form(&$variables) {
  $variables['intro_text'] = 'Hieronder kan u uw e-mailadres en/of wachtwoord wijzigen.';
}
