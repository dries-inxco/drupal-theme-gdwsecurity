(function ($, Drupal, window, document, undefined) {


  Drupal.behaviors.colorBox = {
    attach: function (context, settings) {

        if ( $( "#cboxContent" ).length ) {
            //INXCO Form protection
            if ( $('.webform-component--check input').length > 0 ) {
                    $('.webform-component--check input').val('INXCO_Protected');
            }

            if ( $( "#cboxContent #confirmation" ).length ) {
                 $.colorbox.resize();
            }
        }
    }

  };




var width;

$(window).on("load",function(e){
    width = $(window).width();

    if (width <= 480){
        
        if ( $("body.i18n-nl #cta-1, body.i18n-nl #cta-2").length ) {
             $("#cta-1, #cta-2").off("click").attr("href", "/nl/contact#block-views-core-form-conv");
        }
        if ( $("body.i18n-fr #cta-1, body.i18n-fr #cta-2").length ) {
             $("#cta-1, #cta-2").off("click").attr("href", "/fr/contact#block-views-core-form-conv");
        }
        if ( $("body.i18n-en #cta-1, body.i18n-en #cta-2").length ) {
             $("#cta-1, #cta-2").off("click").attr("href", "/en/contact#block-views-core-form-conv");
        }    
    }
});

$(window).on("load resize",function(e){

    width = $(window).width();

    if (width <= 960){
        MoveContent('#block-block-16','#navigation .menu-block-1 ul');
        $('#navigation .menu-block-1 ul').append($('#block-block-16').detach());	
		
		//Extra Shadow class when navigation is open (William)				
		$('#navtoggle').click(function(){
    		if($(this).is(":checked")) {
        		$('header > div.nav').addClass("shadow");
    		} else {
        		$('header > div.nav').removeClass("shadow");
			}
		});
		
		
    }else{
        //MoveContent('#block-block-16','#top .col.s12');
        $('#top .col.s12 #block-block-13').before($('#navigation .menu-block-1 ul #block-block-16').detach());
    }
    
});

function MoveContent(srcId, destId) {

    //$(destId).append($(srcId).detach());
    $(destId).append($(srcId).detach());
}



    //make teaser clicks actually happen
    Drupal.behaviors.teaserClicks = {
        attach : function(context, settings) {
            $('.node-teaser.node-offer', context).each(function(){
                //alert('test');
                var me = $(this);
                me.click(function(){
                    window.location = me.find('a.button').attr('href');
                });
                // me.find('footer.links').each(function() {
                //     me.click(function(e){
                //         e.stopPropagation();
                //     });
                // });
                me.addClass('teaser-hover');
            });

        }
    }

    // //make overview clicks actually happen
    // Drupal.behaviors.overviewClicks = {
    //     attach : function(context, settings) {
    //         $('.node-overview', context).each(function(){
    //             //alert('test');
    //             var me = $(this);
    //             me.click(function(){
    //                 window.location = me.find('a.button').attr('href');
    //             });
    //             // me.find('footer.links').each(function() {
    //             //     me.click(function(e){
    //             //         e.stopPropagation();
    //             //     });
    //             // });
    //             me.addClass('overview-hover');
    //         });

    //     }
    // }


    $(document).ready(function() {




        //INXCO Form protection
        if ( jQuery('.webform-component--check input').length > 0 ) {
                jQuery('.webform-component--check input').val('INXCO_Protected');
        }


        //smooth anchor scrolling
        $('#content a').click(function(){
            if( $(this).attr("href").indexOf("#") == 0) {
                if ( jQuery($(this).attr("href")).length > 0 ) {
                        $('html, body').animate({
                                scrollTop: $( $(this).attr('href') ).offset().top
                        }, 500);
                        return false;
                }
            }
        });
        
        $('#bottom a#totop').click(function(){
                        $("html, body").animate({ scrollTop: 0 }, "500");
                        return false;
        });



        //filters
        if ( jQuery('.node-type-project-section, .node-type-news-section').length > 0 ) {
            var qs = (function(a) {
                if (a == "") return {};
                var b = {};
                for (var i = 0; i < a.length; ++i)
                {
                    var p=a[i].split('=');
                    if (p.length != 2) continue;
                    b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
                }
                return b;
            })(window.location.search.substr(1).split('&'));


            //get active tag
            if ( qs.hasOwnProperty('tag') ) {
                 //alert('a.tag-'+qs['tag'].charAt(0)+qs['tag'].substr(1));
                jQuery('a.tag-'+qs['tag'].charAt(0)+qs['tag'].substr(1)).addClass('active');
            }
            //get active year
            if ( qs.hasOwnProperty('year') ) {
                jQuery('a.year-'+qs['year'].charAt(0)+qs['year'].substr(1)).addClass('active');
            }
            //get active domain
            if ( qs.hasOwnProperty('domain') ) {
                 //alert('a.tag-'+qs['tag'].charAt(0)+qs['tag'].substr(1));
                jQuery('a.domain-'+qs['domain'].charAt(0)+qs['domain'].substr(1)).addClass('active');
            }


        }        

        

});})(jQuery, Drupal, this, this.document);

// if ( $( "#cboxContent #confirmation" ).length ) {
//      $.colorbox.resize();
// }