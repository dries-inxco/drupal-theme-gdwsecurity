(function ($, Drupal, window, document, undefined) {


	$(document).ready(function() {

	//if ($(window).width() <= 700) {
		$(".flexnav").flexNav();
	//}

	if ($('.notification').length != 0){
		//alert($('.notification').attr('id'));
		var notification_id = $('.notification').attr('id');
		if ($.cookie(notification_id) == null){
			$('.notification').notify();
			$('.notification .close').click(function () {
				$.cookie(notification_id, "1", { expires: 7 });
			});
		}  
	}

	//filter
	if ( jQuery('.client_section, .news_section').length > 0 ) {
		var qs = (function(a) {
			if (a == "") return {};
			var b = {};
			for (var i = 0; i < a.length; ++i)
			{
				var p=a[i].split('=');
				if (p.length != 2) continue;
				b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
			}
			return b;
		})(window.location.search.substr(1).split('&'));


	}



	if ( jQuery('.blog.full, .landingpage.full').length > 0 ) {


		//facebook
		(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/nl_NL/all.js#xfbml=1&appId=599131226823703";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));



		(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));


		//twitter
		!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');


		//google plus
		      window.___gcfg = {
		        lang: 'nl-BE'
		      };

		      (function() {
		        var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
		        po.src = 'https://apis.google.com/js/plusone.js';
		        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
		      })();


		//pinterest
		(function(d){
		    var f = d.getElementsByTagName('SCRIPT')[0], p = d.createElement('SCRIPT');
		    p.type = 'text/javascript';
		    p.async = true;
		    p.src = '//assets.pinterest.com/js/pinit.js';
		    f.parentNode.insertBefore(p, f);
		}(document));(function(d){
		    var f = d.getElementsByTagName('SCRIPT')[0], p = d.createElement('SCRIPT');
		    p.type = 'text/javascript';
		    p.async = true;
		    p.src = '//assets.pinterest.com/js/pinit.js';
		    f.parentNode.insertBefore(p, f);
		}(document));

	}
});})(jQuery, Drupal, this, this.document);



